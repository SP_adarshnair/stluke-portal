## Setting up Portal UI - Angular 7 Project

Step 1 : Install Visual studio code
Step 2 : Install Node.js in the system; use this link: https://nodejs.org/en/
         Note:- you can check the successful installation of the node by typing npm or node in the Command prompt; if install successfully it will display the the version of  node installed 
		 
Step 3 : Install angular-cli , you can use a command npm install -g @angular/cli in Visual code integrated terminal

step 4 : Install all the package dependencies of the project  using command  "npm install"

Step 5 : Run the project using command "ng serve -o"



## Web Api Project - .Net Core Project

Step 1: Install Visual Studio 2017 with .Net core; 
        Note:- In case .Net core is not install in VS 2017, It can be installed using Visual studio Installer.
			   Select  .Net core and click modify. 
Step 2: open the Web Api project in Visual Studio 2017
Step 3: Build and run the project using F5
		